#! /bin/sh

if [ $# -lt 1 ] || [ ! -f 'grades.csv' ]; then
	printf 'Usage: %s [FILE]...\n' "$(basename $0)" >&2
	printf 'Supply file(s) to be distributed into the respective Submission attachment(s)\n' >&2
	printf "Execute within the directory containing 'grades.csv'\n" >&2
	exit 1
fi

files=$@
find \
	-name 'Submission attachment(s)' \
	-exec cp "$files" {} \;
