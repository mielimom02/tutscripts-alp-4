#! /bin/sh

# Requires poppler for pdftops and ghostscript for ps2pdf

if [ $# -gt 2 ]; then
    printf "Usage: %s [input.pdf] [output.pdf]\n" $(basename $0) >&2
    printf "Use '-' for stdin/stdout respectively\n" >&2
    exit 1
fi

if [ $# -gt 0 ]; then
    ifile="$1"
else
    ifile=-
fi

if [ $# -gt 1 ]; then
    ofile="$2"
else
    ofile=-
fi

pdftops "$ifile" - | ps2pdf - "$ofile"
