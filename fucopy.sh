#! /bin/sh

if [ $# -ne 1 ] || [ ! -f 'grades.csv' ]; then
    printf "Usage: %s <pattern>\n" $(basename $0) >&2
    printf "Supply a pattern matching files to be copied, e.g. '*.pdf'\n" >&2
    printf "Execute within the directory containing 'grades.csv'\n" >&2
    exit 1
fi
pattern=$1

find \
    -wholename "*/Submission attachment(s)/$pattern" \
    -execdir cp {} '../Feedback Attachment(s)' \;
