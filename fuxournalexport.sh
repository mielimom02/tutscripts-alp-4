#! /bin/sh

xournal=${XOURNAL:-xournalpp}
if [ $# -ne 1 ] || [ ! -f 'grades.csv' ]; then
    printf "Usage: %s <list of suffix(es)>\n" $(basename $0) >&2
    printf "Supply a suffix to be appended to the name of the exported file\n" >&2
    printf "E.g., %s '_graded'\n" $(basename $0) >&2
    printf "Execute within the directory containing 'grades.csv'\n" >&2
    exit 1
fi
if ! command -v $xournal; then
    printf "The command '%s' is not available\n" "$xournal" >&2
    printf "You can use the 'XOURNAL' environment variable to set an alternative program to the default 'xournalpp'\n" >&2
    printf "E.g., \`export XOURNAL='flatpak run com.github.xournalpp.xournalpp'\`\n" >&2
    exit 1
fi
suffix=$1

find \
    -wholename "*/Submission attachment(s)/*.xopp" \
    -execdir sh -c "fname=\$1; fname=\${fname%.xopp}; echo \"exporting \$PWD\$fname.xopp\"; $xournal -p \"../Feedback Attachment(s)/\${fname}$suffix.pdf\" \"\$fname.xopp\";" sh {} \;
