#!/bin/sh

if [ $# -gt 2 ] || [ "$1" = "--help" ]; then
    printf "Usage: %s [branch] [reponame]\n" $(basename $0) >&2
    printf "Default branch name will be read from comments.txt\n" >&2
    printf "Default reponame will be npvp-exercises\n" >&2
    printf "set env:FU_ANDORRA_USER and optionally env:FU_ANDORRA_DIR to checkout and run on andorra automatically\n" >&2
    printf "if env:FU_ANDORRA_DIR is not set, a npvp-tmp dir will be created in your main dir\n" >&2
    printf "set env:FU_RUN={0,1,2} to 0 = make, 1 = python, 3 = nodejs\n when running on andorra (default is make)\n" >&2
    exit 1
fi

current_dir=$(basename "$PWD")

if [ -z $2 ]; then
    repo_name=`echo $current_dir | sed 's/.*(\([a-z0-9]*\))$/\1/'`
    repo_name="$repo_name/npvp-exercises"
else
    repo_name=$2
fi

if [ -z $1 ]; then 
    # read branch name from comments.txt
    branch_name=`cat "${current_dir}_submissionText.html" | sed -e 's/<[^>]*>//g'` # remove all html tags.
         
else
    branch_name=$1
fi

if [ -z $branch_name ]; then
    printf "No branch name provided via submissionText.html\n" >&2
    exit 1;
fi

case ${FU_RUN:-0} in
    1)
        run_cmd="python3 main.py"
        ;;
    2)
        run_cmd="node main.js" # I hope this is correct, we will see when the sheets are finalised.
        ;;
    *)
        run_cmd="make run"
        ;;
esac

if ! [ -z $FU_ANDORRA_USER ]; then
    # FU_ANDORRA_USER is set, executing on andorra
    clone_dir=${FU_ANDORRA_DIR:-npvp-tmp}
        
    `ssh $FU_ANDORRA_USER@andorra.imp.fu-berlin.de \
        "git clone -b $branch_name git@git.imp.fu-berlin.de:$repo_name ./$clone_dir || exit 1; cd $clone_dir && $run_cmd; cd .. && rm -rf $clone_dir"`
else
    # run locally:
    git clone -b $branch_name git@git.imp.fu-berlin.de:$repo_name git-solution || exit 1
    cd git-solution && $run_cmd; cd ..
fi
