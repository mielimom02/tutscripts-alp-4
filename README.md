# tutscripts

## Requirements

Currently, next to standard core utils, Xournal++ is required for the
recommended workflow; poppler (`pdftops`) and ghostscript (`ps2pdf`)
for the alternative workflow.
In most cases, these are likely already installed.

You will need to use the English version of the Whiteboard. Otherwise, remember
to edit the scripts accordingly.

## Installation

```
$ ./install.sh
```

## Recommended Workflow: Xournal++

1. In the Whiteboard, download the students uploads via **Assignments**, click
   **Grade** on the assignments in the **Assignment List** you want to grade, e.g.,
   **Uebung 01**.  In the top bar, click **Download All** and select the **All**
   checkbox.  Hit **Download** to download the **bulk_download.zip** file.  Extract
   it to a convenient location using `unzip`:
   ```
   $ unzip bulk_download.zip
   ```
2. Enter the directory and copy all submissions *that you want to directly edit*,
   such as source code files, into the **Feedback Attachment(s)** directory.
   PDFs I recommend editing via xournal which creates `.xopp` files and won't touch
   the original PDF, as the XOPP files are "overlays" -- we will merge the
   XOPP with the PDF for upload later.
   ```
   $ cd "Uebung 01"
   $ fucopy '*.asm'
   ```

   Now edit the source code and use Xournal++ annotations to write comments on the
   PDF to add your comments there as well.  Be sure to also enter the grade in the
   "grades.csv" file, e.g., using Libre Office.


<details open>
   <summary>NPVP Workflow extension</summary>

### NPVP
   When correcting npvp exercises, you can use the `fugit` script to automatically 
   pull the correct repository and branch. This requires the submissions to follow some requirements:

   1. the repository has to be named `npvp-exercises`
   1. the submission must include the branch in the comments.txt file
   1. the same student that submitted the exercise must be the owner of the git repo (namespace)

   Futhermore you can autmatically test the code on andorra by setting `FU_ANDORRA_USER` before running `fugit`.
   When running on andorra, a remote directory to clone into can also be  specified using `FU_ANDORRA_DIR`. The cloned
   repository is removed after trying to run in either case (default dir: `npvp-tmp`).
   
   To switch between the different languages and build systems, you can set `FU_RUN` to `0` for `make run`, `1`
   for `python3 main.py` or `2` for `node main.js` (default: `0`).

   examples:
   ```sh
    FU_ANDORRA_USER=fuuser FU_RUN=1 fugit
   ```
   or locally:
   ```sh
   fugit # also runs make run in this case 
   fugitclean
   ```
</details>

3. Now we export final PDFs from the `.xopp` files into the "Feedback Attachment(s)"
   directory.
   We run this command from the directory containing all students' submissions,
   the same that contains the `grades.csv`, this will take some time:
   ```
   $ fuxournalexport '_graded'
   ```
   For every file `foo.pdf` and `foo.xopp` this will create file `foo_graded.pdf`
   containing your feedback.
4. We can now create a ZIP file from the directory again to upload.
   Since we usually don't want to upload the `.xopp` files, this script allows
   us to specify an allowlist of patterns, e.g.:
   ```
   $ cd ..
   $ fuzip "Uebung 01" "*_graded.pdf" "*.asm" "*.csv"
   ```
   This will create a "Uebung 01.zip" file from the "Uebung 01" directory containing
   only graded PDF, ASM and CSV files.
   
   In the Whiteboard select the "Upload All" button, check "All" (or only
   "Grade file" and "Feedback Attachment(s)") and hit "Upload".  You can now check
   whether everything worked fine.  If you want to assign all yet-ungraded students
   the grade 0, you can do that via the text field next to "Apply".  Note, however,
   that in order to do that you need the permission "Edit Assignment", ask your
   supervisor for that.  The final step is to "Release Grades" which will make your
   edits visible to the students.

## Alternative Workflow: Direct PDF Annotations (e.g., Okular)

1. The same as above
2. Here, also copy `*.pdf` files into the **Feedback Attachment(s)** directory:
   ```
   $ cd "Uebung 01"
   $ fucopy '*.pdf'
   ```
3. Instead of exporting the PDFs (we've edited them directly), we need to run
   a fix, as the annotations are still stored as an overlay but "within the
   PDF file".
   Unfortunately, some of the students' PDF readers don't support such
   nnotations, we will "bake them in":
   ```
   $ fufixpdf
   ```
   This will run for some time.
4. We can again use the `fuzip` util.
   Alternatively, as we don't have `.xopp` files to not upload, we can just
   use `zip` directly:
   ```
   $ cd ..
   $ zip -r -9 "bulk_upload-Uebung-01.zip" "Uebung 01"
   ```
