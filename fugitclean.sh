#!/bin/sh
# this script cleans all checked out repositories when called from the same folder that contains the grades.csv
# or a singular checked out repo when called from within a student folder

if [ $# -gt 1 ]; then 
    printf "Usage: fugitclean\m" >&2
    printf "cleans all checked out student solutions when called from the grades.csv folder\n" >&2
    printf "removes a singular checked repo when called from within a students folder" >&2
    exit 1
fi

if [ -z $FU_ANDORRA_USER ]; then 
    # only clean up when not run on ssh
    # ssh cleans up itself :)

find -type d \
    -wholename "*/git-solution" -prune \
    -execdir sh -c "rm -rf ./git-solution/" {} \;
    
fi
