#! /bin/sh

scripts="fixpdf.sh fucopy.sh fufixpdf.sh fuzip.sh fuxournalexport.sh fugit.sh fugitclean.sh"
destdir="${DESTDIR:=$HOME/.local/bin}"
mkdir -p "$destdir"

for s in $scripts; do
    ln -sf "$PWD/$s" "$destdir/${s%.*}"
done

printf "Make sure '%s' is in your path, e.g. add \n" "$destdir" >&2
printf "\texport PATH=%s:\$PATH\n" "$destdir" >&2
printf "to your ~/.profile\n" >&2
